const express = require('express');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const config = require('./config');
const nbaHeightsRoutes= require('./routes/nbaHeights.route');
const swaggerOptions = require('./docs/swaggerOptions');
const swaggerSpecs = swaggerJsDoc(swaggerOptions);
const app = express();

app.use(express.json());

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs));

app.use('/nba_heights', nbaHeightsRoutes);

app.listen(config.module.PORT, () => { console.log(`Escuchando desde el puerto:  http://localhost:${config.module.PORT}`); });

module.exports = app;
