const swaggerOptions = {
    definition: {
      openapi: '3.0.0',
      info: {
        title: 'ApiNBA',
        description: 'Prueba Tecnica NodeJs',
        version: '1.0.0',
      },
      servers: [
        {
          url: 'http://localhost:3000',
          description: 'Local Server',
        },
      ]
    },
    apis: ['./src/routes/*.js'],
  
  };
  
  module.exports = swaggerOptions;
  