const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../index');

chai.should();
chai.use(chaiHttp);

describe('Receive pair of players: ',()=>{
  it('should receive pair of players', (done) => {
	    chai.request(app)
        .get('/nba_heights/139')
        .end((err, response) => {
          response.should.have.status(200);
          response.body.should.be.a('array');
        done();
        });
  });
});

describe('Error in the reception of the pair of players: ',()=>{
  it('should receive: No matches Found', (done) => {
      chai.request(app)
        .get('/nba_heights/0')
        .end((err, response) => {
          response.should.have.status(400);
          response.body.should.be.a('string');
          response.body.should.eq('No matches Found');
        done();
        });
  });
});