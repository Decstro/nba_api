const express = require('express');
const fetch = require('node-fetch');
const router = express();

router.use(express.json());

/**
 * @swagger
 * /nba_heights/{height}:
 *   get:
 *       summary: Receive Pair of NBA Players
 *       tags: [Players]
 *       parameters:
 *         - name: height
 *           in: path
 *           required: true
 *           description: Height to reach
 *           schema:
 *               type: string
 *       responses:
 *           200:
 *               description: Show pairs 
 *               content:
 *                   application/json:
 *                       schema:
 *                           type: array
 *                           items:
 *                               $ref: '#/components/schemas/nbaplayers'
 *           400:
 *               description: No Matches Found
 */

const jsonToArray = (json_data) => {
  const array = Object.keys(json_data).map((key) => [key, json_data[key]]);; 
  const nba_players = array[0][1];
  return nba_players;
};

const pairFinder = (players, height) => {
  let sumPlayers = 0;
  let cont = 0;
  let pairPlayers = [];  
  
  for (let i = 0; i < players.length; i++) {
    for (let j = i; j < players.length - 1; j++) {
      let player1 = players[i];
      let player2 = players[j+1];
      let pair = [player1, player2];
      sumPlayers = parseInt(player1.h_in) + parseInt(player2.h_in);
      if(sumPlayers == height){
        cont += 1;
        pairPlayers.push(pair);
      }
    }
  }  
  if(cont === 0) return false;
  else return pairPlayers;
};

router.get('/:height', async (req, res) => {
  const { height} = req.params;
  const response = await fetch('https://mach-eight.uc.r.appspot.com/');
  const data = await response.json();
  const nba_players = jsonToArray(data);
  const result = pairFinder(nba_players, parseInt(height));

  if(result) res.status(200).json(result);
  else res.status(400).json('No matches Found');
});
  
/**
 * @swagger
 * tags:
 *  name: Players
 *  description: Player's Info
 * components:
 *  schemas:
 *      nbaplayers:
 *          type: object
 *          properties:
 *              first_name:
 *                  type: string
 *                  description: Player's name
 *              h_in:
 *                  type: string
 *                  description: Player's height in inches
 *              h_meters:
 *                  type: string
 *                  description: Player's height in meters          
 *              last_name:
 *                  type: string
 *                  description: Player's Lastname
 *          example:
 *              [{"first_name":"Brevin","h_in":"70","h_meters":"1.78","last_name":"Knight"},{"first_name":"Nate","h_in":"69","h_meters":"1.75","last_name":"Robinson"},]
 *
 */

module.exports = router;
