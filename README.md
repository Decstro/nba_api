# <font color="Darkblue"> API NBA</font>
 
## <font color="Darkblue">Description :page_facing_up:</font>
This API receives an integer input and downloads from an NBA website the list of all players whose height in inches adds up the integer received into the apllication and if no matches are found the application will send the message 'No matches found'.

## <font color="Darkblue"> Technologies :computer: </font>
This api was made using mainly the language <font color="Blue">**Javascript**</font> and using <font color="Blue">NodeJs, ExpressJs, Npm, Swagger, Fetch</font> and <font color="Blue">Mocha</font> to perform the API testing. 


## <font color="Darkblue"> Installation :hammer: </font>
This API uses an environment variable through a .env file and has a testing routine, the environment variable is the server port which is PORT = 3000 (generally these variables are hidden but this is a technical test) . The first thing to do is download the repository and install the project packages in the folder that you decide (it is preferable to name it ApiNBA), for this you must go to the console of your code editor and place the following command:

> <font color="Darkblue"> **npm install** </font> 


### <font color="Darkblue"> Start Testing :clipboard: </font>
To do the testing we must use the command:
> <font color="Darkblue"> **npx mocha src/test/*.js** </font>

## <font color="Darkblue"> Using the API :black_nib: </font>
To use this API we place the following command:

> <font color="darkblue"> **node src/index.js** </font>  

## <font color="Darkblue"> Swagger Documentation </font>
For more information regarding models, schemes and endpoints, it is recommended to review the swagger documentation of this API: [Swagger Documentation Api-NBA](http://localhost:3000/api-docs/).


<font color="Darkblue">**Note:**</font>If we run the test and the api at the same time, an error will be generated, this because the test requests an endpoint from the API and this endpoint requires a server that is already being used.
